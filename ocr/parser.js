const { DIGITS } = require("./digits");

const CHARACTER_WIDTH = 3;
const CHARACTER_HEIGHT = 3;

/**
 * @param {string} fileData
 * @returns {string[]}
 */
function getAllEntries(fileData) {
  const rows = fileData.split("\n");

  const listOfStrings = [];
  while (rows.length >= CHARACTER_WIDTH) {
    // get each number
    listOfStrings.push(rows.splice(0, CHARACTER_HEIGHT).join("\n"));

    // gap between numbers
    rows.splice(0, 1);
  }

  return listOfStrings;
}

/**
 * @param {string} entry
 * @returns {string}
 */
function getNumberFromEntry(entry) {
  // we have 3 lines of numbers
  let entryRows = entry.split("\n");

  // The first three characters of each entry into an individual digit.
  let number = "";

  const NUM_LENGTH = entryRows[0].length / CHARACTER_WIDTH;

  for (let i = 0; i < NUM_LENGTH; i++) {
    const digitPosition = i * CHARACTER_WIDTH;

    // Form a digit of CHARACTER_WIDTH x CHARACTER_HEIGHT
    const digit = Array(CHARACTER_HEIGHT)
      .fill("")
      .reduce(
        (acc, _, idx) =>
          acc + entryRows[idx].substr(digitPosition, CHARACTER_WIDTH),
        ""
      );

    if (DIGITS[digit]) {
      number += DIGITS[digit];
    } else {
      return "Error in data";
    }
  }
  return number;
}

/**
 * @param {string} fileData
 * @returns {string[]}
 */
function parse(fileData) {
  const listOfStrings = getAllEntries(fileData);

  const result = listOfStrings.map(getNumberFromEntry);

  return result;
}

module.exports = {
  parse,
  getNumberFromEntry,
};
