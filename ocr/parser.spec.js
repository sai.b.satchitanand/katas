const { parse, getNumberFromEntry } = require("./parser");

describe("test ocr", () => {
  describe("getNumberFromEntry:", () => {
    it("should get number from a given entry", () => {
      const entry =
        "    _  _     _  _  _  _  _ \n" +
        "  | _| _||_||_ |_   ||_||_|\n" +
        "  ||_  _|  | _||_|  ||_| _|";

      const number = getNumberFromEntry(entry);
      expect(number).toBe("123456789");
    });

    it("should give error in parsing when some characters are invalid", () => {
      const entry =
        "    _  _     _  _  \n" +
        "  | _M _||_||_ |_  \n" +
        "  ||_  _K  | _||_| ";

      const number = getNumberFromEntry(entry);
      expect(number).toBe("Error in data");
    });
  });

  describe("parse: with multiple entries", () => {
    it("should parse all numbers", () => {
      const entries =
        "    _  _     _  _  _  _  _ \n" +
        "  | _| _||_||_ |_   ||_||_|\n" +
        "  ||_  _|  | _||_|  ||_| _|\n" +
        "\n" +
        " _  _  _ \n" +
        "  ||_||_|\n" +
        "  ||_| _|\n";

      const result = parse(entries);
      expect(result).toStrictEqual(["123456789", "789"]);
    });
  });
});
