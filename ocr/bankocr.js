const fs = require("fs");
const { parse } = require("./parser");

const inputFile = process.argv[2];

fs.readFile(inputFile, "utf8", (err, data) => {
  if (err) {
    console.error(`error reading file ${inputFile}: ${err}`);
    process.exit(1);
  }

  const result = parse(data);
  result.forEach((val) => console.log(val));
});
