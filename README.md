Pre-requisite to run the code:

Install the following

- nodejs lts
- yarn

Install the dependencies:(for installing test framework)

```sh
yarn
```

To run tests of the problems:

```sh
yarn test
```

To run loc problem

- Run tests and modify the test data to check results

```sh
yarn test loc
```

To run bankocr problem

- Run tests and modify the test data to check results

```sh
yarn test ocr
```

or

```sh
cd ocr
node bankocr.js sample.txt
```
