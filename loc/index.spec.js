const { countLocOfCSharpCode } = require("./");
const SampleCode = require("./sample");

describe("loc", () => {
  it("when comments and multiple comments are present", () => {
    const result = countLocOfCSharpCode(SampleCode.simpleCodeWithComments);

    expect(result).toStrictEqual({ loc: 11, locCommentsAndWhiteSpaces: 5 });
  });

  it("when code has multi line string", () => {
    const result = countLocOfCSharpCode(SampleCode.multiLineString);

    expect(result).toStrictEqual({ loc: 18, locCommentsAndWhiteSpaces: 1 });
  });

  it("when comments has multi line string", () => {
    const result = countLocOfCSharpCode(SampleCode.multiLineStringInComments);

    expect(result).toStrictEqual({ loc: 18, locCommentsAndWhiteSpaces: 10 });
  });

  it("should give correct loc for all cases", () => {
    const result = countLocOfCSharpCode(SampleCode.allCases);

    expect(result).toStrictEqual({ loc: 29, locCommentsAndWhiteSpaces: 20 });
  });
});
