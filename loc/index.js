/**
 * @param {string} inputSrc
 * @returns {string}
 */
function replaceMultiLineStringWithPlaceholder(inputSrc) {
  const regExForMultiLineString = new RegExp(/@"[\s\S]*?"/, "gm");

  const multiLineStringSrcArr = inputSrc.match(regExForMultiLineString);
  multiLineStringSrcArr &&
    multiLineStringSrcArr.forEach((multiLineStr) => {
      const multiLineStrLOC = multiLineStr.split("\n").length - 1;

      inputSrc = inputSrc.replace(
        multiLineStr,
        `"${Array(multiLineStrLOC).fill("\n").join("PLACEHOLDER")}"`
      );
    });

  return inputSrc;
}

/**
 * @param {string} inputSrc
 * @returns {string}
 */
function replaceWhiteSpace(inputSrc) {
  return inputSrc
    .split("\n")
    .filter((line) => line.trim().length > 0)
    .join("\n");
}

/**
 * @param {string} inputSrc
 * @returns {{
    loc: number;
    locCommentsAndWhiteSpaces: number;
  }}
 */
function countLocOfCSharpCode(inputSrc) {
  const inputLoc = inputSrc.split("\n").length;
  const regExForComments = new RegExp(/\/\*[\s\S]*?\*\/|([^:]|^)\/\/.*$/, "gm");

  const codeWithMultiLineStrPlaceholder = replaceMultiLineStringWithPlaceholder(
    inputSrc
  );
  const codeWithoutComments = codeWithMultiLineStrPlaceholder.replace(
    regExForComments,
    ""
  );
  const codeWithoutEmptySpaces = replaceWhiteSpace(codeWithoutComments);
  const loc = codeWithoutEmptySpaces.split("\n").length;

  return {
    loc,
    locCommentsAndWhiteSpaces: inputLoc - loc,
  };
}

module.exports = {
  countLocOfCSharpCode,
};
