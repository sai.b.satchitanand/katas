module.exports = {
  simpleCodeWithComments: `using System;

  // I'm single line comment
  /*
  im multiline comment
  */
  namespace HelloWorld
  {
    class Program
    {
      static void Main(string[] args)
      {
        Console.WriteLine("Hello World!");    
      }
    }
  }`,

  multiLineString: `using System;

  namespace HelloWorld
  {
    class Program
    {
      static void Main(string[] args)
      {
        string multiLineStr = @"im a string with
            multiline and 
            //comment is part of loc
            /*
            multi line comment is too
            */
            ";
        Console.WriteLine(multiLineStr);    
      }
    }
  }`,

  multiLineStringInComments: `using System;

  namespace HelloWorld
  {
    class Program
    {
      static void Main(string[] args)
      {
        /*
        string multiLineStr = @"im a string with
            multiline and 
            //comment is part of loc
            /*
            multi line comment is too
            */
            ";
        */
        string multiLineStr = @"im a string with
            multiline and 
            //comment is part of loc
            /*
            multi line comment is too
            */
            ";
        Console.WriteLine(multiLineStr);    
      }
    }
  }`,

  allCases: `// C# program to demonstrate the single  
  // line and multiline comments 
  using System; 
    
  namespace HelloApp { 
        
    class Hello {  
      
        // Single Line Comment -- Function to print Message 
        public static void Message(string message) 
        { 
            Console.WriteLine(message); 
        } 
          
        // Main function 
        static void Main(string[] args) 
        { 
              
            /* Multiline Comment -- 
              Define a variable of 
              string type and assign 
              value to it
              @"im a string with
            multiline inside comment line"
              */
            string msg = "single line string"; // "what about me"
  
            string multiLineStr = @"im a string with
            multiline and 
            //comment is part of loc
            /*
            multi line comment is too
            */
            ";
  
            string multiLineStr2 = @"im second string with
            multiline and 
            //comment is part of loc
            /*
            multi line comment is too
            */
            ";
  
            // Calling function 
            Message(msg); 
            Console./* "in the middle" */WriteLine("Hello/*");
        } 
    } 
  } `,
};
